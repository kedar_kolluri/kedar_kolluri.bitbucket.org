
## Is the coin fair?

We flip a coin 20 ($ N $) times and get a result of head 12 ($ H $) times, we would like to determine whether this coin is fair.  

The most simple procedure is to determine the frequency of heads,
$ p_H = 12/20 = 0.6 $
and conclude that the coin is not fair.

But, we **know** that this is most likely a deficient way to make this conclusion. We **know from experience** that we will not consistantly get the value of $ p_H = 0.6 $. For example, we know from experience that **it is possible for the heads to not show up 6 times the next 10 times** we flip it.  In fact, we know that even in a fair coin we can not expect to flip heads 5 out of 10 times every time.

So, we ask, can we do better?  
But what is "better" and what can we "do"?

## May be we can ask a different question
Given that flipping this coin 20 times resulted in 12 heads, what is the **chance** that this is a fair coin?  
If the chance that this coin is fair is very low, maybe we can say it is more likely that the coin is not fair, given this data.

A simple way to find the **chance** that this result is from a fair coin is to take a fair coin, repeat this experiment many many many times and count the fraction of times we got the above result and **may be** we can infer from that number.

Below we are doing the following:
1. Randomly pick a real number uniformly between 0 and 1 20 times.
2. In each of those 20 times
    * if the random number is less than 0.5, we say the coin flipped tail (0)
    * if the random number is greater than or equal to 0.5, we say coin flipped head (1)
3. Resulting number of times heads was observed was noted


```python
import numpy as np
rand = np.random.RandomState(200)
import seaborn as sns
%matplotlib inline
from matplotlib import pyplot
def get_samples1(n=20, samples=10000, p1=0.5):  
    def round_local(x, p=p1):
        if x>p:
            return 0
        else:
            return 1

    array = np.zeros(shape=(n+1,))
    array2 = []
    for i in range(0, int(samples)):
        val = int(np.sum( round_local(rand.uniform()) for item in range(0, n)))
        array2.append(val)
        array[val] = array[val] + 1
    return (array, np.array(array2))
N = 20
samples = 10000
array, array2 = get_samples1(N, samples)
pyplot.plot(range(0, N+1), array/np.sum(array), "o-")
pyplot.ylabel("fraction of times a value was observed in {0:d} repetitions".format(samples))
pyplot.xlabel("Number of heads in {0:d} flips".format(N))
pyplot.xticks(range(0, N+1,int(N/20)));
```


![png](output_2_0.png)


Above plot shows fraction of times we would get each possible result from flipping **a fair coin** 20 times.  
We can conclude that *if we flipped a fair coin 20 times, we are likely to see 12 heads show up for 12% of those times*

This is about the time this gets confusing. So, we should take a moment to recall what we are doing.
Our result is the number of times heads showed up when we flipped a coin 20 times. The act of flipping a coin 20 times is our single experiment.

Above, we perform this experiment 10,000 times. `N` above is the number of times we flip the coin in *each experiment*. And `samples` is the number of times we do the experiment of **flipping the coin `N` times**. For this example, 10,000 is as good as $ \infty $. Don't believe, change $ N $ to something greater and greater and check and convince yourself that 10,000 is as good as $ \infty $).



So, given that a fair coin flipped 20 times shows up heads 12 times 12% of the time, is our coin fair?  
¯\_(ツ)_/¯ ¯\_(ツ)_/¯ ¯\_(ツ)_/¯ ¯\_(ツ)_/¯ ¯\_(ツ)_/¯ ¯\_(ツ)_/¯

In statistics, this is $ p $. People like to use $ p = 0.05 (5\%) $ as cutoff to decide whether a coin is fair. 5% is mostly from tradition. Nothing sacrosant about it.


```python
pyplot.plot(range(0, N+1), np.cumsum(array)/np.sum(array), "o-")
pyplot.ylabel("fraction of times observed heads is less than the one shown for {0:d} repetitions".format(samples))
pyplot.xlabel("Number of heads in {0:d} flips".format(N))
pyplot.xticks(range(0, N+1,int(N/20)));
pyplot.yticks(np.array(range(0, 101, 10))*1.0/100);
```


![png](output_6_0.png)


## Maybe we can ask a different question

Instead of checking how often a fair coin gives the result we got from our coin, lets compute the **fraction of times** the result we got will show up **for any kind of coin**.

A coin with one probability can be considered as "one kind of coin".

Since, probability of a coin flipping head any one time can be anywhere between 0 (always tails) to 1 (always heads), lets run the above "simulation" for probabilities between 0 and 1.  
In practice, we divide the range between 0 and 1 into some number of pieces, say a 100. Run the above "simulation" for each of those points and compute the fraction of times 12 comes up.


```python
def compute_across_p(N=20, h=12, samples=10000, grids=100):
    prob_array = np.zeros(shape=(grids+1,))
    val_array = np.zeros(shape=(grids+1,))
    for i in range(0, grids+1):
        p = i*1.0/grids
        array, array2 = get_samples1(N, samples, p)
        prob_array[i] = array[h]/np.sum(array)
        val_array[i] = p
    return (val_array, prob_array)

probs, res = compute_across_p()
pyplot.plot(probs, res, "o");
pyplot.xticks(np.array(range(0, 101, 10))*1.0/100);
```


![png](output_8_0.png)


From above plot, we can see the "most likely probability" of the coin that returns this result is around 0.6, which is intutive.  
But we also see that any value between 0.4 and 0.8 has some chance of the result showing up.


```python
pyplot.figure(figsize=(10,6))
pyplot.plot(probs, np.cumsum(res)/np.sum(res));
pyplot.ylim((-0.01,1.01))
pyplot.xticks(np.array(range(0, 101, 5))*1.0/100);
pyplot.yticks(np.array(range(0, 101, 5))*1.0/100);
```


![png](output_10_0.png)
